Radio Javan Downloader
----------------------

**Description**

This is a python script that can generate download urls from radiojavan.com playlists.

**Install**

This script needs below libs to work:

-urllib<br />
-requests<br />
-json<br />
-argparse<br />
-BeautifulSoup<br />

* you can install it with `pip install **LibName**`

**Usage**

`python3 RadioJavanDownloader.py --url https://www.radiojavan.com/playlists/playlist/mp3/233dd24ead2a --out links.txt
`

**Todo**

- Download single song
- Telegram Bot